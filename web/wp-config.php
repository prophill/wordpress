<?php
/**
 * The configuration for WordPress on Prøpelled
 *
 * Warning!
 *   This file is was created for and by Prøpelled, these settings may be optimized
 *   to run in a Prøpelled environment.
 *
 *   If you want to adjust wp-config to run outside Prøpelled, we recommended
 *   that you start from a fresh wp-config-sample.php
 *   @see https://codex.wordpress.org/Editing_wp-config.php#Default_wp-config-sample.php
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define( 'WP_DEBUG', false );
// ** MySQL settings ** //
define('DB_NAME', $_ENV['MYSQL_DATABASE']);
define('DB_USER', $_ENV['MYSQL_USER']);
define('DB_PASSWORD', $_ENV['MYSQL_PASSWORD']);
define('DB_HOST', $_ENV['MYSQL_HOST']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** WordPress Database Table prefix. Don't change this if on Prøpelled! */
$table_prefix  = 'wp_';

// ** Authentication Unique Keys and Salts. ** //
// WP_KEYS_HERE
define('AUTH_KEY',         'U/}i|]W`?D%|}L>XP)mF:Z9CRprv9YgTW9-l7/@[=5ZG.-LSQ35xo,zX^$%VOsXu');
define('SECURE_AUTH_KEY',  'e9gNv?%#h:({65|&P+Gf5h7f2mCV9$I3RLE`YLpfO^/CDT=3ut!2#UR{<[=I!J-}');
define('LOGGED_IN_KEY',    '-]-u<xO7`ajU?,;BO<e~c~v_7dpi{TWZM./XB_;//<T,?f>7=.Ptx4BK^r-z(EN.');
define('NONCE_KEY',        'usk~&,@EhIY]$yl@i&4=?u^a@A2m3GN>S}nc#`H||>rL65_/1IO!vb? R&ka8uj.');
define('AUTH_SALT',        '-v+cKAp+&;-kS6iX^6Q6GjHWMSyjM43nB?RxB_mR 2xnHFu<Z(|M@&#~F=hbGJ$f');
define('SECURE_AUTH_SALT', '>l#l0G[wD? TiCM``4,GH@.<Pmy*~bu/h.pL=Mm-a4^~d@6 ~X1}e$y0MCpI&EGr');
define('LOGGED_IN_SALT',   'Z2nMLZT| +}<|Fc1uX|:(;l`4I!wPa7ex)Ilw~52tx-j7|I0mo)CM$46yvDSAudE');
define('NONCE_SALT',       '6]KexL-es[mG1a4{O5YY7yAQK*Ra,5`NTgZRlU!{-jYuH?6Q?OBo9K6#E5cUf@Ei');

/** Makes sure the scripts in wp-admin is seperate files. */
define('CONCATENATE_SCRIPTS', false);

/**
 * Setup Redis for Redis plugins
 */
if (isset($_ENV['REDIS_HOST'])) {
    define('WP_REDIS_SCHEME', 'tcp');
    define('WP_REDIS_HOST', $_ENV['REDIS_HOST']);
    define('WP_REDIS_PORT', isset($_ENV['REDIS_PORT']) ? $_ENV['REDIS_PORT'] : '6379');
    define('WP_REDIS_DATABASE', isset($_ENV['REDIS_DATABASE']) ? $_ENV['REDIS_DATABASE'] : '1');
}

/** Force wordpress to do file operations directly. */
define('FS_METHOD', 'direct');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/**
 * Sort out http(s) behind reverse proxy.
 * Security is assumed to be handled at an earlier stage.
 */
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $_SERVER['HTTPS'] = 'on';
    $_SERVER['SERVER_PORT'] = 443;
}

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
